package de.tnt.co2_di_re.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import java.util.ArrayList;
import java.util.List;

import de.tnt.co2_di_re.R;
import de.tnt.co2_di_re.model.Controller;
import de.tnt.co2_di_re.model.Days;
import de.tnt.co2_di_re.model.Verkehrsmittel;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Referenzen hier
    }

    @Override
    protected void onResume() {
        super.onResume();

        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.viewport);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);
        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        // specify an adapter (and feed data)
        List<Days> lst = Controller.getInstance().getListFromUser();
        mAdapter = new ListTripAdapter(lst);
        recyclerView.setAdapter(mAdapter);

        final Button btn = findViewById(R.id.add);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, AddActivity.class));
            }
        });


    }
}
