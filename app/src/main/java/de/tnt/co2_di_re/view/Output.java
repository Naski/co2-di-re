package de.tnt.co2_di_re.view;
import java.util.*;

public class Output {
    private List<String> sentences = new ArrayList<String>();
    private static int tabLength = 0;
    private static Output out = null;
    private Output(int tabLength){  
        this.tabLength = tabLength;
    }

    public static Output getOutput() {
        if(out == null) {
            out = new Output(tabLength);
        }
        return out;
    }
    public void add(String input) {
        sentences.add(input);
    }
    private String build() {
        
        String result = "";
        int lineLength = 0;
        for(String obj : sentences) {
            String[] sentence = obj.split(" ");
            for(String word : sentence) {
                lineLength+=word.length();
                if(lineLength < tabLength) {
                    result+=word+" ";
                } else {
                    lineLength = 0;
                    result+="\n "+word+" ";
                }
                
            }
            result+="\n";
        }
        return result;
    }
    public void print() {
        System.out.println(build());
        flush();
    }
    private void flush() {
        sentences.clear();
    }
    public void setTabLength(int value) {
        
        this.tabLength = value;
    }
    
}
