package de.tnt.co2_di_re.view;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import de.tnt.co2_di_re.R;
import de.tnt.co2_di_re.model.Days;
import de.tnt.co2_di_re.model.Fortbewegung;
import java.util.List;
public class ListTripAdapter extends RecyclerView.Adapter<ListTripAdapter.ListSingleView> {
    private List<Days> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ListSingleView extends RecyclerView.ViewHolder {
        // each data item
        public LinearLayout itemBlock;
        public CardView cardView;
        public TextView textViewDate;
        public TextView textViewCo2;

        public ListSingleView(View v) {
            super(v);
            itemBlock = (LinearLayout) v.findViewById(R.id.cardUnit);
            cardView = (CardView) v.findViewById(R.id.card_view);
            textViewDate = (TextView) v.findViewById(R.id.textViewDate);
            textViewCo2 = (TextView) v.findViewById(R.id.textViewCo2);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ListTripAdapter(List<Days> daysList) {
        mDataset = daysList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ListSingleView onCreateViewHolder(ViewGroup parent,
                                             int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview, parent, false);
        ListSingleView vh = new ListSingleView(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ListSingleView holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Log.d("FriendlyMessage", "Heyo im here, how are you");
        Days day = mDataset.get(position);
        holder.textViewDate.setText(day.printDate());
        holder.textViewCo2.setText(day.printTotalCo2());
        // change CardViews Background Color based on total emission count
	// CardView card = holder.cardView;

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
