package de.tnt.co2_di_re.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Spinner;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import de.tnt.co2_di_re.R;
import de.tnt.co2_di_re.model.Controller;


public class AddActivity extends AppCompatActivity {
    private String[] fahrzeugSpinner = Controller.getInstance().getKeysOfCo2List();
    Calendar date = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat("d/MM/yyyy");
    private String newDate = sdf.format(date.getTime());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        final EditText grundTextField = findViewById(R.id.grund);

        grundTextField.setOnClickListener (new EditText.OnClickListener() {

            @Override
            public void onClick(View v) {
                grundTextField.getText().clear();
            }
        });
        final EditText distanzTextField = findViewById(R.id.distanz);
        Log.d("date",newDate);
        final CalendarView calendarField = findViewById(R.id.calendarView);

        calendarField.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                month = month +1;
                newDate = dayOfMonth+"/"+month+"/"+year;
            }
        });


        final Spinner s = findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, fahrzeugSpinner);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s.setAdapter(adapter);

        final Button submit = findViewById(R.id.submit);
        //1000 is debug val for date
        submit.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                Controller.getInstance().addEntryToUser(grundTextField.getText().toString(), Integer.parseInt(distanzTextField.getText().toString()), s.getSelectedItem().toString(), newDate);
                Intent i= new Intent(AddActivity.this, MainActivity.class);
                //i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(i);
            }
        });


    }
}
