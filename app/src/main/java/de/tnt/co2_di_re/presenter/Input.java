package de.tnt.co2_di_re.presenter;
import java.util.*;
import de.tnt.co2_di_re.model.IinputObserver;
import de.tnt.co2_di_re.view.Output;

public class Input
{
    private Output out = Output.getOutput();
    private IinputObserver observer = null;
    
    public Input() {
        
        }


    //Singleton reimplementieren
    public void getNextInput() {
        Scanner sc = new Scanner(System.in);
        String uInput = sc.next();
        switch(uInput) {
                case "add": {
                    //Test
                    out.add("Wie bist du gereist?");
                    out.print();
                    String fahrzeug = sc.next().toLowerCase();
                    out.add("Wie weit bist du gefahren?");
                    out.print();
                    int distance = sc.nextInt();
                    observer.notifyAdd(fahrzeug, distance);
                    break;
                }
                case "list": {
                    observer.notifyList();
                    break;
                }
                case "stop": {
                    observer.notifyStop();
                    break;
                }
                default: {
                    observer.notifyError(uInput);
                }
            }
        }
        public void addObserver(IinputObserver observer) {
            this.observer = observer;
        }
        
        
}
