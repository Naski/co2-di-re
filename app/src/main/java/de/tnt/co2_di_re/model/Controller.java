package de.tnt.co2_di_re.model;
import android.util.Log;

import java.util.*;

public class Controller {
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    private User user = new User();
    private boolean running = true;
    private Map <String, Integer> co2Map = new HashMap<>();
    private static Controller con = null;




    private Controller() {


        //Instanzvariable initialisieren
       co2Map.put("Auto", 139);
       co2Map.put("Bus Nahverkehr",75);
       co2Map.put("Bus Fernverkehr", 32);
       co2Map.put("Fahrrad", 0);
       co2Map.put("Flugzeug", 201);
       co2Map.put("Zu Fuß", 0);
       co2Map.put("Zug Nahverkehr", 60);
       co2Map.put("Zug Fernverkehr", 36);

    }
    //Its ugly, refactor
    public void addEntryToUser(String reason, int distance, String fahrzeug, String date) {
        Log.d("EntryAdded", reason+","+distance+","+fahrzeug+","+date);
        user.travelForDate(reason, distance, new Verkehrsmittel(fahrzeug, co2Map.get(fahrzeug)), date);
    }
    public List<Days> getListFromUser() {
        return user.getDays();
    }
    public static Controller getInstance(){
        if(con == null) {
            con = new Controller();
        }
        return con;
    }
    public int getCo2ValueOf(String vehicleType) {
        return co2Map.get(vehicleType);
    }
    public String[] getKeysOfCo2List() {
        return co2Map.keySet().toArray(new String[co2Map.size()]);
    }
}
