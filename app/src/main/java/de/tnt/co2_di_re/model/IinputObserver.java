package de.tnt.co2_di_re.model;

public interface IinputObserver
{
    public void notifyAdd(String fahrzeug, int distance);
    public void notifyList();
    public void notifyStop();
    public void notifyError(String uInput);
}
