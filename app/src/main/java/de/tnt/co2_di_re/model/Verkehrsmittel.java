package de.tnt.co2_di_re.model;

import java.util.ArrayList;
import java.util.List;

public class Verkehrsmittel {

    private String name;
    private int co2Value;

    public Verkehrsmittel(String name, int co2Value){
       this.name = name;
       this.co2Value = co2Value;
   }
    public int getCo2(){
        return co2Value;
    }
    public String getName(){
        return name;
    }
}
