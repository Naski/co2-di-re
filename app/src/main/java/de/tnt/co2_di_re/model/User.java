package de.tnt.co2_di_re.model;

import java.util.*;

import de.tnt.co2_di_re.model.Verkehrsmittel;

public class User
{
    
    private List<Days> days = new ArrayList<Days>();

    public User()
    {
        
    }
    
    public void travelForDate(String reason, int distanz, Verkehrsmittel mittel, String date){
        if(!dateExists(date)) {
            Days day = new Days(date);
            day.addMovement(new Fortbewegung(reason, distanz, mittel));
            days.add(day);
        } else {
            days.get(getDateId(date)).addMovement(new Fortbewegung(reason, distanz, mittel));
        }

        
    }
    public float calculateCo2ForDay(int date) {
        float result = 0;
        List<Fortbewegung> lst = days.get(date).getMovements();
        for(Fortbewegung obj : lst) {
            result+=obj.co2Insgesamt();
        }
        return result;
    }
    
    public List<Days> getDays() {
        return days;
    }
    private boolean dateExists(String date) {
        for(Days obj : days) {
            if(obj.date.equals(date)) {
                return true;
            }
        }
        return false;
    }

    //Watchout, Returns 0 on error
    private int getDateId(String date) {
        for(int i=0;i<days.size();i++) {
            if(days.get(i).date.equals(date)) {
                return i;
            }
        }
        return 0;
    }
}
