package de.tnt.co2_di_re.model;

import java.util.ArrayList;
import java.util.List;

public class Days {

    String date = "";
    private List<Fortbewegung> movements = new ArrayList<Fortbewegung>();
    public Days(String date) {
        this.date = date;

    }

    public void addMovement(Fortbewegung movement) {
        movements.add(movement);
    }
    public Fortbewegung getMovementAt(int val) {
        return movements.get(val);
    }
    public List<Fortbewegung> getMovements() {
        return movements;
    }
    public String printDate() {
        return date;
    }
    public String printTotalCo2() {
        int result = 0;
        for(Fortbewegung obj : movements) {
            result+=obj.co2Insgesamt();
        }
        return result+"g";
    }
}
