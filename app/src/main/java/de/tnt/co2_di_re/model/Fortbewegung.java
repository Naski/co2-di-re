package de.tnt.co2_di_re.model;

public class Fortbewegung {

    private int distanz;
    private Verkehrsmittel mittel;
    private String reason;


    public Fortbewegung(String reason, int distanz, Verkehrsmittel mittel) {
        this.reason = reason;
        this.distanz = distanz;
        this.mittel = mittel;
    }
    public float co2Insgesamt() {
        return (float)distanz*mittel.getCo2();
    }
    public int getDistanz() {
        return distanz;
    }

}
